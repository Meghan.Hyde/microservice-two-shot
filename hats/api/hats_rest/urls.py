from .views import api_list_hats, api_show_location
from django.urls import path, include

urlpatterns = [
    path("hats/", api_list_hats, name="api_list_hats"),
    path("hats/<int:pk>/", api_show_location, name="api_show_location")
]
