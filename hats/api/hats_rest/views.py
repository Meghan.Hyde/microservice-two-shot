from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Hat, LocationVO
import json

class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = ["closet_name", "import_href"]

class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "style",
        "fabric",
        "color",
        "picture",
        "id",
        "location",
    ]
    encoders = {
        "location": LocationVODetailEncoder(),
    }

# class HatDetailEncoder(ModelEncoder):
#     model = Hat
#     properties = [
#         "style",
#         "fabric",
#         "color",
#         "picture"
#     ]
#     encoders = {
#         "location": LocationVODetailEncoder()
#     }

@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        print('hats', hats)
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
        )
    else:
        content = json.loads(request.body)
        print('content:', content)
        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location

        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatListEncoder,
            status=201,
            safe=False,
        )

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_location(request, pk):
    """
    """
    if request.method == "GET":
        hat = Hat.objects.get(id=pk)
        return JsonResponse(
            hat,
            encoder=HatListEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse(
            {"deleted": count > 0}
        )
