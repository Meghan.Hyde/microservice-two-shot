from django.shortcuts import render
from common.json import ModelEncoder
from .models import Shoe, BinVO
from django.views.decorators.http import require_http_methods
import json
from django.http import JsonResponse


class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = ["closet_name", "import_href"] # using href as Bin Model is in another microservice and href is the most detailed way to grab


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = ["manufacturer", "name", "id", "color", "picture_url"]

    def get_extra_data(self, o):
        return {"bin": o.bin.closet_name} #this adds the bin closet name to the shoe list data


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "name",
        "color",
        "picture_url"
    ]
    encoders = {
        "bin": BinVODetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
        )
    else: #POST request
        content = json.loads(request.body)
        # print('Content:', content)
        try:
            bin_href = content["bin"]
            # print('Content:', bin_href)
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin

        except BinVO.DoesNotExist:
            return JsonResponse({"message": "Invalid Bin ID"}, status=400)

        # print('debugging:', content)

        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE"])
def api_show_delete_shoe(request, id):
    if request.method == "GET":
        shoe = Shoe.objects.get(id=id)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Shoe.objects.filter(id=id).delete()
        return JsonResponse(
            {"deleted": count > 0}
        )
