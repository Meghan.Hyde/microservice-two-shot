from django.db import models
from django.urls import reverse


class BinVO(models.Model):
    # closet_name = models.CharField(max_length=100)
    closet_name = models.CharField(max_length=100, null=True)
    import_href = models.CharField(max_length=200, unique=True)


class Shoe(models.Model):
    manufacturer = models.CharField(max_length=50)
    name = models.CharField(max_length=50)
    color = models.CharField(max_length=25)
    picture_url = models.URLField(null=True)
    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.PROTECT,
    )
    def __str__(self):
        return self.name

