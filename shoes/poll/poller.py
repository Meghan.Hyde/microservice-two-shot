import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shoes_project.settings")
django.setup()

from shoes_rest.models import BinVO
# Import models from shoes_rest, here.
# from shoes_rest.models import Something

def get_bins(): #communication between shoes and wardrobe
    binUrl = "http://wardrobe-api:8000/api/bins/"
    response = requests.get(binUrl)
    content = json.loads(response.content)
    for bin in content["bins"]: # going through list of bins
        BinVO.objects.update_or_create( # checking BinVO that href exists or update stuff in defaults or if not created will create
            import_href=bin["href"],
            defaults={"closet_name": bin["closet_name"]}, # key has to match name in Model and value name has to match
            # the Bin model in the wardrobe app
        )



def poll():
    while True:
        print('Shoes poller polling for data')
        try:
            get_bins()
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__": #telling def poll() to run
    poll()
