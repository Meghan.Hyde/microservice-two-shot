# Wardrobify

Team:

* Meghan Hyde - Shoe microservice
* Person 2 - Which microservice?

* Person 2 - Which microservice?
Tyler Valentine - Hat microservice
## Design

## Shoes microservice

Models:
- I created a Shoe model for my shoes that had all the features required - manufacturer, name, color, picture url and bin as the foreign key.
- I also created a binVO since the bin model is in a seperate wardrobe microservice.

Integration:
I integrated the models using encoders in my api_views. I created a binVO detail encoder along with a list and detail shoe encoder that I could use in my views. For my views I created a GET list of shoes, GET a detailed view of a single shoe, DELETE a shoe and POST or create a new shoe. All views directly tie to the wardrobe's bin model / feature as I have to list what closet the shoe will go in.

Initially I did not create a show detail view in Insomnia, so due to that I added more features to my shoe list encoder and simplified my code in ShoeList.js > function ShoeList. Looking back I could have left my code that looped through the detailed view and just added that quickly to my Insomnia but talking with SEIR Alex, we went a different way in getting the code to work.

APIs:
Shoe API: http://localhost:8080/api/shoes/

GET list of shoes: "  "  " (uses the shoe api as is). Here is what the data looks like that you will receive back from Insomnia or in your browser.
{
			"manufacturer": "converse",
			"name": "mid-top all stars",
			"id": 2,
			"color": "black",
			"picture_url": "https://media.kohlsimg.com/is/image/kohls/3517313_Black_White?wid=805&hei=805&op_sharpen=1",
			"bin": "first closet"
		},

GET to show a detailed shoe: Shoe API/int/
Here is the data you would receive back in Insomnia.

{
	"manufacturer": "converse",
	"name": "mid-top all stars",
	"color": "black",
	"picture_url": "https://media.kohlsimg.com/is/image/kohls/3517313_Black_White?wid=805&hei=805&op_sharpen=1"
}

POST / create a new shoe uses the Shoe API.
The structure of the data you send in looks like this:

{
	"manufacturer": "Vans",
	"name": "Old Skool Cozy Hug Sherpa Shoe",
	"color": "mustard yellow",
	"picture_url": "https://images.vans.com/is/image/Vans/VN0007NT_1M7_ALT1?hei=617&wid=492&qlt=50&resMode=sharp2&op_sum=0.9,1.0,8,0",
	"bin": "/api/bins/1/"
}

and what the data you would receive from Insomnia would look like this:
{
	"manufacturer": "Vans",
	"name": "Old Skool Cozy Hug Sherpa Shoe",
	"color": "mustard yellow",
	"picture_url": "https://images.vans.com/is/image/Vans/VN0007NT_1M7_ALT1?hei=617&wid=492&qlt=50&resMode=sharp2&op_sum=0.9,1.0,8,0"
}

DELETE shoe uses the Shoe API/int/
You should receive a boolean back showing the shoe was deleted:
{
	"deleted": true
}

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.

I've got two models, LocationVO and Hat.
The Hat model has the attributes of fabric, style, color, picture and a foreign key of location.

LocationVO has the attributes of import_href and closet_name. import_href is there so we can pull the Location model from the wardrobe microservice.
