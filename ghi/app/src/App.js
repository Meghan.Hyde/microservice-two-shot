import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
// import { useState } from 'react';
import HatList from './HatList';
// import HatForm from './HatForm';

import ShoeForm from './ShoeForm';
import ShoeList from './ShoeList';
// import ShoeListV2 from './ShoeListV2';

function App() {

// if (shoes === undefined) {
//   return null;
// }


  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats" element={<HatList />} />
          <Route path="shoes">
            <Route path="new" element={<ShoeForm />} />
            <Route index element={<ShoeList />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );

}

export default App;
