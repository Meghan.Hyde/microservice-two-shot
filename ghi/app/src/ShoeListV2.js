import { useEffect, useState } from 'react';

function ShoeListV2() {

  const handleClickDelete = async (shoe) => {
    console.log('Shoe:', shoe)
    const shoeUrl = `http://localhost:8080/api/shoes/${shoe.id}/`;
    const response = await fetch(shoeUrl, {
      method: 'DELETE',
    });

    if (response.ok) {
      const data = await response.json()
      console.log('Data:', data);
      getShoes(data.shoes)
    }

  }

    const [shoes, setShoes] = useState([]);

    async function getShoes() {
      const shoeUrl = 'http://localhost:8080/api/shoes/';
      const response = await fetch(shoeUrl)
      if (response.ok) {
          const data = await response.json();
          setShoes(data.shoes)

      };
    }

    useEffect(() => {
      getShoes();
   }, [] );

return (

    <table className="table table-striped table-hover">
      <thead className='thead table-success'>
        <tr>
          <th>Manufacturer</th>
          <th>Name</th>
          <th>Color</th>
          <th>Picture Url</th>
          <th>Bin</th>
        </tr>
      </thead>
      <tbody>
      {shoes.map(shoe => {
        return (
        <tr key={shoe.id}>
          <td>{ shoe.manufacturer }</td>
          <td>{ shoe.name}</td>
          <td>{ shoe.color}</td>
          <td><img src={ shoe.picture_url} className='img-thumbnail'/></td>
          <td>{ shoe.bin }</td>
          {/* <button type="button" class="btn btn-primary"
          onClick={() => shoe.delete(shoe.href)}>Delete</button> */}
          <td><button type="button" className="btn btn-info"
          onClick={() => handleClickDelete(shoe)}>Delete</button></td>
        </tr>

      );
    })}
      </tbody>
    </table>

    );
}

export default ShoeListV2;


// # using id instead of href because I don't have the reverse on the model
