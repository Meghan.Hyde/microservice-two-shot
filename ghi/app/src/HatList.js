import React, { useEffect, useState } from "react";
import HatForm from './HatForm';

function HatList() {
    const [hats, setHats] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/hats/');

        if (response.ok) {
            const data = await response.json();
            setHats(data.hats)
        }
    };

    useEffect(() => {
        getData();
    }, []);


    const handleDelete = async (hatId) => {
        const response = await fetch(`http://localhost:8090/api/hats/${hatId}`, {
            method: 'DELETE',
        });
        if (response.ok) {
            getData();
        }
    }

    return (
        <div>
            <HatForm />

        <table className='table table-striped'>
            <thead>
                <tr>
                    <th>Hat</th>
                    <th>Color</th>
                    <th>Fabric</th>
                    <th>Picture</th>
                    <th>Location</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                {hats.map(hat => {
                    return (
                        <tr key={hat.id}>
                            <td>{ hat.style }</td>
                            <td>{ hat.color }</td>
                            <td>{ hat.fabric }</td>
                            <td><img src={hat.picture} alt={hat.style} style={{ maxWidth: '100px' }}/></td>
                            <td>{ hat.location.closet_name }</td>
                            <td>
                                <button className="btn btn-danger" onClick={() => handleDelete(hat.id)}>Delete</button>
                                {/* <button onClick={() => this.delete(hat.id)}>Delete</button> */}
                            </td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
      </div>
    );
}

export default HatList;
