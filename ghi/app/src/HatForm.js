import React, { useState, useEffect } from 'react';

function HatForm() {
  const [locations, setLocations] = useState([]);
  const [formData, setFormData] = useState({
    style: '',
    fabric: '',
    color: '',
    picture: '',
    location: '',
    // id: ''
  });

  const getData = async () => {
    const url = 'http://localhost:8100/api/locations/'
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      console.log('getData Data:', data)
      setLocations(data.locations);
      console.log('setLocations, location state:' ,locations)
    }
  };

  useEffect(() => {
    getData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    console.log('Form Data:', formData);
    const url = `http://localhost:8090/api/hats/`;

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(url, fetchConfig);
    console.log('handleSubmit response object:', response)
    if (response.ok) {
      setFormData({
        style: '',
        fabric: '',
        color: '',
        picture: '',
        location: '',
        // id: ''
      });
    }
  };

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;

    setFormData({
      ...formData,
      [inputName]: value
    });
  };


  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new hat</h1>
          <form onSubmit={handleSubmit} id="create-conference-form">
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                value={formData.style}
                placeholder="Style"
                required
                type="text"
                name="style"
                id="style"
                className="form-control"
              />
              <label htmlFor="style">Style</label>
            </div>
            <div className="mb-3">
              <label htmlFor="fabric">Fabric</label>
              <textarea
                onChange={handleFormChange}
                value={formData.fabric}
                className="form-control"
                id="fabric"
                rows="3"
                name="fabric"
              ></textarea>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                value={formData.color}
                placeholder="Color"
                required
                type="text"
                name="color"
                id="color"
                className="form-control"
              />
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                value={formData.picture}
                placeholder="Picture URL"
                required
                type="URL"
                name="picture"
                id="picture"
                className="form-control"
              />
              <label htmlFor="picture">Picture URL</label>
            </div>
            <div className="mb-3">
              <select
                onChange={handleFormChange}
                value={formData.location}
                required
                name="location"
                id="location"
                className="form-select"
              >
                <option value="">Choose a location</option>
                {locations.map((location) => (
                  <option key={location.href} value={location.href}>
                    {location.closet_name}
                  </option>
                ))}
              </select>
            </div>
            <button type="submit" className="btn btn-primary">
              Create
            </button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default HatForm;
