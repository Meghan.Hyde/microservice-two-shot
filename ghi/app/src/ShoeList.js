import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import './styles.css';

function ShoeColumn(props){
    // console.log('Props:', props.list);

    const handleClickDelete = async (shoe) => {
      // console.log('Shoe:', shoe)
      const shoeUrl = `http://localhost:8080/api/shoes/${shoe.id}/`;
      const response = await fetch(shoeUrl, {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json',
        }
      });

      if (response.ok) {
        const data = await response.json()
        // console.log('Data:', data);
        props.getShoes();
      }

    }


    return (
        <div className="col">
      {props.list.map(shoe => {
        // console.log('Data:', data)
        return (
          <div key={shoe.href} className="card mb-3 shadow">
            <img
              src={shoe.picture_url}
              className="card-img-top"
            />
            <div className="card-body">
              <h5 className="card-title">{shoe.manufacturer} | {shoe.name}</h5>
              <h6 className="card-title ">{shoe.bin}</h6>
            </div>

          <button type="button" className="btn btn-outline-info"
          onClick={() => handleClickDelete(shoe)}>Delete</button>
          {/* <button type="button" class="btn btn-info" onClick={(handleClickDelete) => this.delete(shoe.id)}>
            Delete</button> */}
          </div>
        );
      })}
    </div>
    );

}

function ShoeList({shoes}) {
    const [shoeColumns, setShoeColumns] = useState([[], [], []]);
    // console.log('Shoes:', shoes)
    async function getShoes() {
        const shoeUrl = 'http://localhost:8080/api/shoes/';
        const response = await fetch(shoeUrl)
        if (response.ok) {
            const data = await response.json();

            try {
                let shoeColumns = [[], [], []];

                let i = 0;
                for (let shoe of data.shoes) { // iterating over GET list of shoes since we weren't required to make a show detail shoe. I could have left previous code and created a show detail shoe in Insomnia
                    // console.log('Shoe:', shoe)
                        shoeColumns[i].push(shoe);
                        i++;
                        if (i > 2) {
                            i = 0; // resetting columns once three entries have been entered
                        }
                }
                setShoeColumns(shoeColumns);

            } catch (error) {
                console.error(error);
            }
        }


    }
    useEffect(() => {
        getShoes();
    }, []);


    return (
          <div className="container text-center">
            <h1 className='custom-font text-info'>Shoe Collection</h1>
            <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
            <Link to="/shoes/new" className="btn btn-outline-info btn-lg px-4 gap-3">Create A Shoe</Link>
          </div>
            <div className="row mt-3">
              {shoeColumns.map((shoeList, index) => {
                return (
                  <ShoeColumn key={index} list={shoeList} getShoes={getShoes} />
                );
              })}
            </div>
          </div>
      );
  }

  export default ShoeList;




